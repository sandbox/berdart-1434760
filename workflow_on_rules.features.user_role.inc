<?php
/**
 * @file
 * workflow_on_rules.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function workflow_on_rules_user_default_roles() {
  $roles = array();

  // Exported role: editor
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => '2',
  );

  return $roles;
}
