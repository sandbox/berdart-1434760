<?php
/**
 * @file
 * workflow_on_rules.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function workflow_on_rules_user_default_permissions() {
  $permissions = array();

  // Exported permission: access private fields
  $permissions['access private fields'] = array(
    'name' => 'access private fields',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: access rules link approve_article
  $permissions['access rules link approve_article'] = array(
    'name' => 'access rules link approve_article',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'rules_link',
  );

  // Exported permission: access rules link publish_article
  $permissions['access rules link publish_article'] = array(
    'name' => 'access rules link publish_article',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'rules_link',
  );

  // Exported permission: access rules link reject_article
  $permissions['access rules link reject_article'] = array(
    'name' => 'access rules link reject_article',
    'roles' => array(),
  );

  // Exported permission: access rules link unpublish_article
  $permissions['access rules link unpublish_article'] = array(
    'name' => 'access rules link unpublish_article',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'rules_link',
  );

  // Exported permission: administer field permissions
  $permissions['administer field permissions'] = array(
    'name' => 'administer field permissions',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create article content
  $permissions['create article content'] = array(
    'name' => 'create article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: create field_status
  $permissions['create field_status'] = array(
    'name' => 'create field_status',
    'roles' => array(),
  );

  // Exported permission: delete any article content
  $permissions['delete any article content'] = array(
    'name' => 'delete any article content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own article content
  $permissions['delete own article content'] = array(
    'name' => 'delete own article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any article content
  $permissions['edit any article content'] = array(
    'name' => 'edit any article content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_status
  $permissions['edit field_status'] = array(
    'name' => 'edit field_status',
    'roles' => array(),
  );

  // Exported permission: edit own article content
  $permissions['edit own article content'] = array(
    'name' => 'edit own article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own field_status
  $permissions['edit own field_status'] = array(
    'name' => 'edit own field_status',
    'roles' => array(),
  );

  // Exported permission: view field_status
  $permissions['view field_status'] = array(
    'name' => 'view field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_status
  $permissions['view own field_status'] = array(
    'name' => 'view own field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own unpublished content
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
