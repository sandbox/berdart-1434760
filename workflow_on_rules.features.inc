<?php
/**
 * @file
 * workflow_on_rules.features.inc
 */

/**
 * Implements hook_views_api().
 */
function workflow_on_rules_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_default_rules_link().
 */
function workflow_on_rules_default_rules_link() {
  $items = array();
  $items['approve_article'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Approve",
      "link_type" : "token",
      "bundles" : { "article" : "article" },
      "entity_link" : 1
    },
    "name" : "approve_article",
    "label" : "Approve article",
    "path" : "approve_article",
    "entity_type" : "node",
    "rdf_mapping" : [  ]
  }');
  $items['publish_article'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Publish",
      "link_type" : "token",
      "bundles" : { "article" : "article" },
      "entity_link" : 1
    },
    "name" : "publish_article",
    "label" : "Publish article",
    "path" : "publish_article",
    "entity_type" : "node",
    "rdf_mapping" : [  ]
  }');
  $items['reject_article'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Reject",
      "link_type" : "token",
      "bundles" : { "article" : "article" },
      "entity_link" : 1
    },
    "name" : "reject_article",
    "label" : "Reject article",
    "path" : "reject_article",
    "entity_type" : "node",
    "rdf_mapping" : [  ]
  }');
  $items['unpublish_article'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Unpublish",
      "link_type" : "token",
      "bundles" : { "article" : "article" },
      "entity_link" : 1
    },
    "name" : "unpublish_article",
    "label" : "Unpublish article",
    "path" : "unpublish_article",
    "entity_type" : "node",
    "rdf_mapping" : [  ]
  }');
  return $items;
}
