<?php
/**
 * @file
 * workflow_on_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function workflow_on_rules_default_rules_configuration() {
  $items = array();
  $items['rules_current_user_is_author'] = entity_import('rules_config', '{ "rules_current_user_is_author" : {
      "LABEL" : "Current user is author of the node",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "AND" : [
        { "data_is" : { "data" : [ "site:current-user" ], "value" : [ "node:author" ] } },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "4" : "4" } }
          }
        }
      ]
    }
  }');
  $items['rules_link_condition_approve_article'] = entity_import('rules_config', '{ "rules_link_condition_approve_article" : {
      "LABEL" : "Rules link: Approve\\u002FReject article condition",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "AND" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3" } }
          }
        },
        { "component_rules_node_has_status" : { "node" : [ "node" ], "status" : "1" } }
      ]
    }
  }');
  $items['rules_link_condition_publish_article'] = entity_import('rules_config', '{ "rules_link_condition_publish_article" : {
      "LABEL" : "Rules link: Publish article condition",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "AND" : [
        { "component_rules_current_user_is_author" : { "node" : [ "node" ] } },
        { "OR" : [
            { "component_rules_node_has_status" : { "node" : [ "node" ], "status" : "2" } },
            { "component_rules_node_has_status" : { "node" : [ "node" ], "status" : "5" } }
          ]
        }
      ]
    }
  }');
  $items['rules_link_condition_reject_article'] = entity_import('rules_config', '{ "rules_link_condition_reject_article" : {
      "LABEL" : "Rules link: Reject article condition",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "AND" : [
        { "component_rules_link_condition_approve_article" : { "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_link_condition_unpublish_article'] = entity_import('rules_config', '{ "rules_link_condition_unpublish_article" : {
      "LABEL" : "Rules link: Unpublish article condition",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "AND" : [
        { "component_rules_current_user_is_author" : { "node" : [ "node" ] } },
        { "component_rules_node_has_status" : { "node" : [ "node" ], "status" : "4" } }
      ]
    }
  }');
  $items['rules_link_set_approve_article'] = entity_import('rules_config', '{ "rules_link_set_approve_article" : {
      "LABEL" : "Rules link: Approve article rules set",
      "PLUGIN" : "rule set",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_status" } },
              { "data_is" : { "data" : [ "node:field-status" ], "value" : "1" } }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "node:field-status" ], "value" : "2" } },
              { "component_rules_notify_author_about_status_changing" : { "node" : [ "node" ] } }
            ],
            "LABEL" : "Approve article"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_publish_article'] = entity_import('rules_config', '{ "rules_link_set_publish_article" : {
      "LABEL" : "Rules link: Publish article rules set",
      "PLUGIN" : "rule set",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_status" } },
              { "OR" : [
                  { "data_is" : { "data" : [ "node:field-status" ], "value" : "5" } },
                  { "data_is" : { "data" : [ "node:field-status" ], "value" : "2" } }
                ]
              }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "node:field-status" ], "value" : "4" } },
              { "node_publish" : { "node" : [ "node" ] } },
              { "component_rules_notify_author_about_status_changing" : { "node" : [ "node" ] } }
            ],
            "LABEL" : "Publish article"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_reject_article'] = entity_import('rules_config', '{ "rules_link_set_reject_article" : {
      "LABEL" : "Rules link: Reject article rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_status" } },
              { "data_is" : { "data" : [ "node:field-status" ], "value" : "1" } }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "node:field-status" ], "value" : "3" } },
              { "component_rules_notify_author_about_status_changing" : { "node" : [ "node" ] } }
            ],
            "LABEL" : "Reject article"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_unpublish_article'] = entity_import('rules_config', '{ "rules_link_set_unpublish_article" : {
      "LABEL" : "Rules link: Unpublish article rules set",
      "PLUGIN" : "rule set",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_status" } },
              { "data_is" : { "data" : [ "node:field-status" ], "value" : "4" } }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "node:field-status" ], "value" : "5" } },
              { "node_unpublish" : { "node" : [ "node" ] } },
              { "component_rules_notify_author_about_status_changing" : { "node" : [ "node" ] } }
            ],
            "LABEL" : "Unpublish article"
          }
        }
      ]
    }
  }');
  $items['rules_node_has_status'] = entity_import('rules_config', '{ "rules_node_has_status" : {
      "LABEL" : "Node has status",
      "PLUGIN" : "and",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "Node", "type" : "node" },
        "status" : { "label" : "Status", "type" : "integer" }
      },
      "AND" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_status" } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : [ "status" ] } }
      ]
    }
  }');
  $items['rules_notify_author_about_status_changing'] = entity_import('rules_config', '{ "rules_notify_author_about_status_changing" : {
      "LABEL" : "Notify author about status changing",
      "PLUGIN" : "action set",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "ACTION SET" : [
        { "drupal_message" : { "message" : "Status changed to [node:field-status]" } }
      ]
    }
  }');
  return $items;
}
